import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleListComponent } from "../article-list/article-list.component";
import { HttpClientModule } from '@angular/common/http';

@Component({
    selector: 'app-root',
    standalone: true,
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    imports: [CommonModule, ArticleListComponent, HttpClientModule]
})
export class AppComponent {
  title = 'article-editor';
}
