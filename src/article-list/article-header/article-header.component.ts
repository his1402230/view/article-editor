import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Article } from '@his-viewmodel/article/src/app/article.interface';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'his-article-header',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './article-header.component.html',
  styleUrls: ['./article-header.component.scss']
})
export class ArticleHeaderComponent implements OnChanges {
  @Input() article!: Article;
  @Output() deleteArticle = new EventEmitter<any>();
  @Output() updateTitle = new EventEmitter<any>();

  public originalArticle!: Article;
  public isEdit = false;

  ngOnChanges(changes: any): void {
    if (changes.article) {
      this.originalArticle = changes.article.currentValue;
      this.article = Object.assign({}, changes.article.currentValue);
    }
  };

  doDeleteArticle() {
    this.deleteArticle.emit(this.article);
  }

  doUpdateTitle() {
    const newArticle: Article = {
      id: this.article.id,
      title: this.article.title,
    }
    this.updateTitle.emit(newArticle);
    this.isEdit = false;
  };

  doCancelUpdateTitle() {
    this.article.title = this.originalArticle.title;
    this.isEdit = false;
  };
}
