import { Component, OnInit, WritableSignal, inject, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleListService } from './article-list.service';
import { Article } from '../../node_modules/@his-viewmodel/article/src/app/article.interface'
import { ArticleHeaderComponent } from "./article-header/article-header.component";
import { ArticleBodyComponent } from "./article-body/article-body.component";

@Component({
    selector: 'his-article-list',
    standalone: true,
    providers: [ArticleListService],
    templateUrl: './article-list.component.html',
    styleUrls: ['./article-list.component.scss'],
    imports: [CommonModule, ArticleHeaderComponent, ArticleBodyComponent]
})
export class ArticleListComponent implements OnInit {
  public articleService: ArticleListService = inject(ArticleListService);
  public articles: WritableSignal<Article[]> = signal([]);

  async ngOnInit() {
    this.articles.set(await this.articleService.getArticles());
  }

  async onDeleteArticle(article: Article) {
    try {
      console.log(article);
      await this.articleService.deleteArticle(article);
      this.articles.mutate(i => {
        i.splice(i.findIndex(i => i.id === article.id), 1);
      });
    }
    catch (error) {
      console.log(error);
    }
  }

  async onUpdateTitle(article: Article) {
    try {
      await this.articleService.updateTitle(article);
      this.articles.mutate(i => {
        i[i.findIndex(i => i.id === article.id)].title = article.title;
      });
    }
    catch(error) {
      console.log(error);
    }
  }
}
