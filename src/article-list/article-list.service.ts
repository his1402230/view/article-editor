import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Article } from '@his-viewmodel/article/src/app/article.interface';
import { lastValueFrom } from 'rxjs';
import { ArticleService } from '@his-viewmodel/article/src/app/article.service';

@Injectable({
  providedIn: 'root'
})
export class ArticleListService implements ArticleService{

  constructor() { }

  private url = 'http://localhost:3001/articles';
  private http: HttpClient = inject(HttpClient);

  async getArticles() {
    const result$ = this.http.get<Article[]>(this.url);
    return await lastValueFrom(result$);
  }

  async deleteArticle(article: Article) {
    const result$ = this.http.delete<Article>(`${this.url}/${article.id}`);
    return await lastValueFrom(result$);
  }

  async updateTitle(article: Article) {
    const result$ = this.http.patch<Article>(`${this.url}/${article.id}`, article);
    return await lastValueFrom(result$);
  }
}
